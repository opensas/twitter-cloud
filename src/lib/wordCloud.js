// import * as d3 from 'd3'
// import * as d3cloud from 'd3-cloud'
// import d3 from "d3"
// see: https://stackoverflow.com/questions/56690468/cannot-get-d3-js-to-work-inside-svelte-component-with-rollup/56710275#56710275
// see: https://stackoverflow.com/questions/57719159/integrate-d3-cloud-with-rollup-actually-svelte

// import * as d3 from 'd3'

const wordCloud = (selector, words, interval, width = 500, height = 500) => {
  const wordsSized = getWords(words, width, height)
  const myWordCloud = SvgWordCloud(selector, width, height)

  const updateCloud = () => {
    myWordCloud.update(wordsSized)
    if (interval) setTimeout(updateCloud, interval)
  }

  updateCloud()
}

function SvgWordCloud(selector, width = 500, height = 500) {

  const fill = d3.scale.category20();

  d3.select(selector).selectAll("*").remove();

  //Construct the word cloud's SVG element
  const svg = d3.select(selector).append('svg')
    .attr('width', width)
    .attr('height', height)
    .append('g')
    .attr('transform', `translate(${width / 2},${height / 2})`)

  //Draw the word cloud
  function draw(words) {
    const cloud = svg.selectAll('g text')
      .data(words, d => d.text)

    //Entering words
    cloud.enter()
      .append('text')
      .style('font-family', 'Impact')
      .style('fill', (d, i) => fill(i))
      .attr('text-anchor', 'middle')
      .attr('font-size', 1)
      .text(d => d.text)

    //Entering and existing words
    cloud
      .transition()
      .duration(600)
      .style('font-size', d => d.size + 'px')
      .attr('transform', d => `translate(${[d.x, d.y]})rotate(${d.rotate})`)
      .style('fill-opacity', 1)

    //Exiting words
    cloud.exit()
      .transition()
      .duration(200)
      .style('fill-opacity', 1e-6)
      .attr('font-size', 1)
      .remove()
  }

  //Use the module pattern to encapsulate the visualisation code. We'll
  // expose only the parts that need to be public.
  return {

    //Recompute the word cloud for a new set of words. This method will
    // asycnhronously call draw when the layout has been computed.
    //The outside world will need to call this function, so make it part
    // of the wordCloud return value.
    update: function (words) {
      d3.layout.cloud().size([width, height])
        .words(words)
        .padding(5)
        .rotate(() => ~~(Math.random() * 2) * 90)
        .font('Impact')
        .fontSize(d => d.size)
        .on('end', draw)
        .start()
    }
  }

}

//Prepare one of the sample sentences by removing punctuation,
// creating an array of words and computing a random size attribute.
function getWords(words, width = 500, height = 500) {
  const counts = words.map(i => i.count)
  const min = Math.min(...counts)
  const max = Math.max(...counts)
  const top = max - min

  const baseSize = width * height * 10 / (500 * 500)// 500 * 500 --> 10    w*h --> x
  const topSize = width * height * 60 / (500 * 500)// 500 * 500 --> 60    w*h --> x

  // return words.map( item => ({ text: item.word, size: 10 + (item.count / top) * 60 }))
  return words.map(item => ({ text: item.word, size: baseSize + (item.count / top) * topSize }))
}


export { wordCloud as default, SvgWordCloud, getWords }
